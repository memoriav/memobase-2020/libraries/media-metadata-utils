# Media Metadata Utils

Releases: [Package registry](https://gitlab.switch.ch/memoriav/memobase-2020/libraries/package-registry/-/packages)

The library provides a notion of a [`ResourceWithLocator`](https://gitlab.switch.ch/memoriav/memobase-2020/libraries/media-metadata-utils/-/blob/master/src/main/scala/ch/memobase/models/ResourceWithLocator.scala#L19), which contains information on the external media resource a record is linking to, if this resource is externally accessible, and with what [player](https://gitlab.switch.ch/memoriav/memobase-2020/libraries/media-metadata-utils/-/blob/master/src/main/scala/ch/memobase/models/MediaPlayer.scala#L19) this resource should be played on Memobase. This helps to decide if the resource should be further processed and how the media server should handle the resource eventually.

Its single concern is to reduce redundancies in the code base of [Media Metadata Extractor](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/media-metadata-extractor) and [Media Metadata Preprocessor](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/media-metadata-preprocessor), thus a broader use is quite pointless.

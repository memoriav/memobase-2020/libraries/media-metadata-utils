import Dependencies.*

ThisBuild / scalaVersion := "3.5.2"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / startYear := Some(2020)
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .enablePlugins(GitlabPlugin)
  .settings(
    name := "Media Metadata Utils",
    assembly / assemblyJarName  := "app.jar",
    assembly / test := {},
    git.baseVersion := "0.1.0",
    git.useGitDescribe := true,
    gitlabProjectId := Some(GitlabProjectId("1324")),
    gitlabDomain := "gitlab.switch.ch",
    libraryDependencies ++= Seq(
      log4jApi % "provided",
      log4jScala % "provided",
      scalaUri,
      scalaTest % Test
    ),
    licenses += ("Apache-2.0", new URI(
      "https://www.apache.org/licenses/LICENSE-2.0.txt"
    ).toURL)
  )

/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.models

sealed trait MediaPlayer {
  val name: String
}

trait MemobasePlayer extends MediaPlayer

case object AudioPlayer extends MemobasePlayer {
  val name = "audio"
}

case object ImageViewer extends MemobasePlayer {
  val name = "image"
}

case object VideoPlayer extends MemobasePlayer {
  val name = "video"
}

case object SRFAudioPlayer extends MediaPlayer {
  val name = "srfaudio"
}

case object SRFVideoPlayer extends MediaPlayer {
  val name = "srfvideo"
}

case object VimeoPlayer extends MediaPlayer {
  val name = "vimeo"
}

case object YoutubePlayer extends MediaPlayer {
  val name = "youtube"
}

case object ZEMPlayer extends MediaPlayer {
  val name = "zem"
}

case object ZHDKPlayer extends MediaPlayer {
  val name = "zhdk"
}

/*
 * Copyright 2021 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase

import ch.memobase.models.*
import io.lemonlabs.uri.*
import org.apache.logging.log4j.scala.Logging

import java.text.ParseException
import scala.util.{Failure, Success, Try}

object LocatorExtraction extends Logging {

  private val EbucoreLocator =
    "<http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#locator>"
  private val RicoType = "<https://www.ica.org/standards/RiC/ontology#type>"

  def filterEnrichableResources(
      resources: List[ResourceWithLocator]
  ): List[ResourceWithLocator] = resources.filter(_.enrichable)

  def getEbucoreLocators(
      key: String,
      msg: String
  ): (String, List[ResourceWithLocator]) = {
    val recordType = getRecordType(msg)
    val ebucoreLocators = msg
      .split("(\r\n|\r|\n)")
      .filter(_.contains(EbucoreLocator))
      .map(line => {
        val elements = line.split(" ", 3)
        val resource = elements(0).substring(1, elements(0).length - 1)
        val locator = elements(2).substring(1, elements(2).length - 3)
        val mediaPlayer = if (isPreviewImage(resource)) {
          ImageViewer
        } else {
          mapToMediaPlayer(locator, recordType)
        }
        ResourceWithLocator(
          key,
          resource,
          locator,
          mediaPlayer,
          mediaPlayer.isInstanceOf[MemobasePlayer] && !blacklistedLocator(
            locator
          )
        )
      })
      .toList
    if (ebucoreLocators.isEmpty) {
      logger.warn(
        "Can't find resource locator in record. Is this a resource-less metadata object?"
      )
    }
    (msg, ebucoreLocators)
  }

  private def blacklistedLocator(locator: String): Boolean = {
    val blacklisted = List(
      "fonoteca.ch"
    )
    getApexDomain(locator)
      .flatMap(ad => Success(blacklisted.contains(ad)))
      .getOrElse(false)
  }

  private def isPreviewImage(uri: String): Boolean = uri.endsWith("/derived")

  private def getRecordType(msg: String): Option[String] =
    msg
      .split("\r\n|\r|\n")
      .collectFirst(isRecordTypeStatement)

  private def isRecordTypeStatement: PartialFunction[String, String] =
    new PartialFunction[String, String] {
      def apply(line: String): String = line.split(" ")(2).replaceAll("\"", "")

      def isDefinedAt(line: String): Boolean = {
        val splitLine = line.split(" ")
        splitLine(0).startsWith("<https://memobase.ch/record") &&
        splitLine(1) == RicoType
      }
    }

  // noinspection ScalaStyle
  private def mapToMediaPlayer(
      mediaUrl: String,
      recordType: Option[String]
  ): MediaPlayer = {
    if (mediaUrl.startsWith("sftp:")) {
      getMemobasePlayerType(recordType)
    } else if (isZHDKModcast(mediaUrl)) {
      ZHDKPlayer
    } else {
      getApexDomain(mediaUrl) match {
        case Success(apexDomain)
            if apexDomain == "srgssr.ch" || apexDomain == "srf.ch" || apexDomain == "rts.ch" =>
          recordType match {
            case Some("Film")         => SRFVideoPlayer
            case Some("Radio")        => SRFAudioPlayer
            case Some("Ton")          => SRFAudioPlayer
            case Some("Tonbildschau") => SRFVideoPlayer
            case Some("TV")           => SRFVideoPlayer
            case Some("Video")        => SRFVideoPlayer
            case Some(x) =>
              logger.warn(
                s"Unknown rico:type $x found! Setting player type to default SRFVideoPlayer"
              )
              SRFVideoPlayer
            case None =>
              logger.warn(
                "No rico:type on record found! Setting player type to default SRFVideoPlayer"
              )
              SRFVideoPlayer
          }

        case Success(apexDomain) if apexDomain == "vimeo.com" => VimeoPlayer
        case Success(apexDomain)
            if apexDomain == "youtu.be" || apexDomain == "youtube.com" =>
          YoutubePlayer
        // TODO: Activate zem player as soon as it is in active use
        // case Success(apexDomain) if apexDomain == "zem.ch" => ZEMPlayer
        case Success(_) =>
          getMemobasePlayerType(recordType)
        case Failure(_) =>
          logger.warn(
            s"Couldn't identify apex domain of url $mediaUrl. Setting player type to default MemobasePlayer"
          )
          getMemobasePlayerType(recordType)
      }
    }
  }

  private def getMemobasePlayerType(recordType: Option[String]): MediaPlayer =
    recordType match {
      case Some("Film")         => VideoPlayer
      case Some("Foto")         => ImageViewer
      case Some("Radio")        => AudioPlayer
      case Some("Ton")          => AudioPlayer
      case Some("Tonbildschau") => VideoPlayer
      case Some("TV")           => VideoPlayer
      case Some("Video")        => VideoPlayer
      case Some(x) =>
        logger.warn(
          s"Unknown rico:type $x found! Setting player type to default VideoPlayer"
        )
        VideoPlayer
      case None =>
        logger.warn(
          "No rico:type on record found! Setting player type to default VideoPlayer"
        )
        VideoPlayer
    }

  private def isZHDKModcast(uri: String): Boolean = {
    Url.parseOption(uri) match {
      case Some(u: AbsoluteUrl) =>
        u.hostOption.map(_.toString).contains("modcast.zhdk.ch")
      case _ => false
    }
  }

  private def getApexDomain(uri: String): Try[String] = {
    Url.parseOption(uri) match {
      case Some(u: AbsoluteUrl) =>
        getHostValue(u.hostOption, uri)
      case Some(_: RelativeUrl) =>
        Success("")
      case Some(_) => Failure(new ParseException(s"Can't handle url $uri", 0))
      case None =>
        Failure(new ParseException(s"Can't parse url $uri", 0))
    }
  }

  private def getHostValue(host: Option[Host], uri: String): Try[String] =
    host match {
      case Some(h: DomainName) =>
        h.apexDomain match {
          case Some(d) => Success(d)
          case None =>
            Failure(
              new ParseException(s"Can't extract apex domain from $uri", 0)
            )
        }
      case Some(ip: IpV4) =>
        Success(ip.value)
      case Some(ip: IpV6) =>
        Success(ip.value)
      case None => Failure(new ParseException(s"Can't parse host from $uri", 0))
    }

}

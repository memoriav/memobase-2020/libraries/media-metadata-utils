/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source
import ch.memobase.models.ZHDKPlayer

class LocatorExtractionTest extends AnyFunSuite {

  private def getTextFromResourcesFolder(filepath: String): String = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    text
  }

  test("media on youtube are not enrichable") {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "some_id",
      getTextFromResourcesFolder("input-msg/yt_media.nt")
    )
    val enrichable =
      LocatorExtraction.filterEnrichableResources(msgWithLocator._2)
    assert(!enrichable.exists(_.enrichable))
  }

  test("media on vimeo are not enrichable") {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "some_id",
      getTextFromResourcesFolder("input-msg/vimeo_media.nt")
    )
    val enrichable =
      LocatorExtraction.filterEnrichableResources(msgWithLocator._2)
    assert(!enrichable.exists(_.enrichable))
  }

  test(
    "remote videos should be enrichable, if they are not on vimeo / youtube / srf"
  ) {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "record_id",
      getTextFromResourcesFolder("input-msg/fhnw.nt")
    )
    val enrichable =
      LocatorExtraction.filterEnrichableResources(msgWithLocator._2)
    assert(enrichable.forall(_.enrichable))
  }

  test("records with missing media url are not enrichable") {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "some_id",
      getTextFromResourcesFolder("input-msg/no_media.nt")
    )
    val enrichable =
      LocatorExtraction.filterEnrichableResources(msgWithLocator._2)
    assert(enrichable.forall(_.enrichable))
  }

  test("locally available media are enrichable") {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "some_id",
      getTextFromResourcesFolder("input-msg/local_media.nt")
    )
    val enrichable =
      LocatorExtraction.filterEnrichableResources(msgWithLocator._2)
    assert(enrichable.forall(_.enrichable))
  }

  test("records with custom sftp link are enrichable") {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "some_id",
      getTextFromResourcesFolder("input-msg/sftp_media.nt")
    )
    val enrichable =
      LocatorExtraction.filterEnrichableResources(msgWithLocator._2)
    assert(enrichable.forall(_.enrichable))
  }

  test("digitalObjects and thumbnails should be extracted separately") {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "some_id",
      getTextFromResourcesFolder("input-msg/local_media_w_thumbnail.nt")
    )
    val enrichable =
      LocatorExtraction.filterEnrichableResources(msgWithLocator._2)
    assert(enrichable.length == 2)
  }

  test("media on zhdk modcast are recognised") {
    val msgWithLocator = LocatorExtraction.getEbucoreLocators(
      "some_id",
      getTextFromResourcesFolder("input-msg/zhdk.nt")
    )
    assert(msgWithLocator._2(1).player == ZHDKPlayer)
  }

}
